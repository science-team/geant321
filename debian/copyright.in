This package was debianized by Kevin B. McCarty <kmccarty@debian.org> and
Lifeng Sun <lifongsun@gmail.com>.  It was downloaded from the web page
http://wwwinfo.cern.ch/asd/cernlib/download/2005_source/tar/


COPYRIGHT
---------

CERNLIB, including GEANT 3.21, is copyright (C) CERN and others.  As of this
writing (1 February 2008), the web page
http://wwwasd.web.cern.ch/wwwasd/cernlib/conditions.html (which gives the terms
under which CERNLIB may be used) states:

  CERNLIB Availability

  (C) Copyright CERN except where explicitly stated otherwise. Permission to
  use and/or redistribute this work is granted under the terms of the GNU
  General Public License. FLUKA routines included in GEANT3 are joint copyright
  of INFN and CERN and are not licensed under the GPL: permission to use and/or
  redistribute outside GEANT3 should be negotiated. The software and
  documentation made available under the terms of this license are provided
  with no warranty.

  Last modified: 18-March-2004

The aforementioned FLUKA routines have been excised from the GEANT 3.21 source
code and binary packages distributed by Debian.

There is some ambiguity as to whether CERN intended to have CERNLIB available
specifically under GPL version 2 (the current version at the time the above-
mentioned web page was written) or always under the current version.  (The text
"GNU General Public License" in the web page quoted above is a hyperlink to
http://www.gnu.org/copyleft/gpl.html which, as of this writing, references GPL
version 3.)  I have requested clarification from the upstream maintainer.  In
the meantime, it should be noted that the orig.tar.gz files for GEANT 3.21 in
Debian were obtained from the upstream web site prior to the release of GPL
version 3.  It therefore seems to me to be safe to assume that re-distribution
of GEANT 3.21 from the Debian source packages is permissible under the terms
either of GPL version 2 or of the current GPL version, at the re-distributor's
option.

On Debian systems, the complete text of the current version of the GNU General
Public License can be found in the file `/usr/share/common-licenses/GPL-3'.
The text of version 2 can be found at `/usr/share/common-licenses/GPL-2'.


EXCEPTIONS
----------

The file geant321.h in the libgeant321-2-dev binary package, and all files
included in the tarball upstream/src_cfortran.tar.gz in the geant321 source
package, are instead

  Copyright (C) 1990-2003  Burkhard Burow, <burow@desy.de>,
  http://www-zeus.desy.de/~burow/cfortran/index.htm

and are licensed (at your choice) under either the GNU Library General Public
License or an alternate non-free license.

On Debian systems, the complete text of the GNU Library General Public License
can be found in the file `/usr/share/common-licenses/LGPL-2'.

The alternate license for the cfortran-related files is as follows:

THIS PACKAGE, I.E. CFORTRAN.H, THIS DOCUMENT, AND THE CFORTRAN.H EXAMPLE
PROGRAMS ARE PROPERTY OF THE AUTHOR WHO RESERVES ALL RIGHTS. THIS PACKAGE AND
THE CODE IT PRODUCES MAY BE FREELY DISTRIBUTED WITHOUT FEES, SUBJECT TO THE
FOLLOWING RESTRICTIONS:
- YOU MUST ACCOMPANY ANY COPIES OR DISTRIBUTION WITH THIS (UNALTERED) NOTICE.
- YOU MAY NOT RECEIVE MONEY FOR THE DISTRIBUTION OR FOR ITS MEDIA
  (E.G. TAPE, DISK, COMPUTER, PAPER.)
- YOU MAY NOT PREVENT OTHERS FROM COPYING IT FREELY.
- YOU MAY NOT DISTRIBUTE MODIFIED VERSIONS WITHOUT CLEARLY DOCUMENTING YOUR
  CHANGES AND NOTIFYING THE AUTHOR.
- YOU MAY NOT MISREPRESENTED THE ORIGIN OF THIS SOFTWARE, EITHER BY EXPLICIT
  CLAIM OR BY OMISSION.

THE INTENT OF THE ABOVE TERMS IS TO ENSURE THAT THE CFORTRAN.H PACKAGE NOT BE
USED FOR PROFIT MAKING ACTIVITIES UNLESS SOME ROYALTY ARRANGEMENT IS ENTERED
INTO WITH ITS AUTHOR.

THIS SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
EXPRESSED OR IMPLIED. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE
SOFTWARE IS WITH YOU. SHOULD THE SOFTWARE PROVE DEFECTIVE, YOU ASSUME THE COST
OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION. THE AUTHOR IS NOT RESPONSIBLE
FOR ANY SUPPORT OR SERVICE OF THE CFORTRAN.H PACKAGE.

                                              Burkhard Burow
                                              burow@desy.de


DELETIA
-------

The following files and directories have been removed from the Debian source
package of GEANT 3.21 due to license ambiguities or incompatibilities with the
GPL (e.g. positive BSD advertising clause).  If needed, they may be found in
the upstream source tarballs.

DEADPOOL_LIST_GOES_HERE
